"""
Функция print_student_marks_by_subject.

Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
"""
def print_student_marks_by_subject(student_name: str, **kwargs):
    print(f'Имя студента: {student_name}')
    for subj,mark in kwargs.items():
        print(f'{subj} : {mark}')

if __name__ == '__main__':
    print_student_marks_by_subject('Вася', math=5, biology=[4, 5], trud=4)
