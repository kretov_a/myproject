"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle

def save_dict_to_file_pickle(my_file, user_dict):
    with open(my_file, 'wb') as f:
        pickle.dump(user_dict, f)

    file = open(my_file, 'rb')
    my_dict = pickle.load(file)
    file.close()

    print(f"user_dict == my_dict: {user_dict == my_dict}")
    print(f"type(user_dict): {type(my_dict)}")
    print(my_dict)

if __name__ == '__main__':
    save_dict_to_file_pickle('my_file.pkl',
                            {2: 3, 3: 4})