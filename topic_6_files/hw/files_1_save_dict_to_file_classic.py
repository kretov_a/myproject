"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, user_dict):
    with open(path, 'w') as f:
        f.write(str(user_dict))

if __name__ == '__main__':
    path = 'les1.txt'
    save_dict_to_file_classic(path, {1: 'df', 2: "вап", 3: 4})

    with open(path, 'r') as f:
        print(f.read())


