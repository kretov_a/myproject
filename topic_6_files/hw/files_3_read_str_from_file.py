"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""
def read_str_from_file(file):

    #with open(path, 'r') as f:
     #   print(str(f.read()))

    #if __name__ == '__main__':
       # path = str('hw3.txt')
        #read_str_from_file(path)
    f1 = open(file, 'r')
    content = f1.read()
    print(content)
    f1.close()
    if __name__ == '__main__':
        file = 'hw3.txt'
        read_str_from_file(file)