"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает кортеж: (
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
).

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
"""
def dict_to_list(my_dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    return (list(my_dict.keys()), list(my_dict.values()), len(my_dict), len(set(my_dict.values())))
def dict_to_list(my_dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    return (list(my_dict.keys()), list(my_dict.values()), len(my_dict), len(set(my_dict.values())))