"""
Функция get_translation_by_word.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то ‘Can`t find Russian word: {word}’.

Если вместо словаря передано что-то другое, то возвращать строку .
Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
"""
def get_translation_by_word(my_dict, my_word):
    if type(my_dict) != dict:
        return 'Dictionary must be dict!'
    if type(my_word) != str:
        return 'Word must be str!'
    if len(my_dict) == 0:
        return 'Dictionary is empty!'
    if len(my_word) == 0:
        return 'Word is empty!'
    for i in my_dict:
        if my_dict.key(i)  == my_word:
            return my_dict.value(i)
def get_translation_by_word(my_dict, word):
    if not isinstance(my_dict, dict):
        return 'Dictionary must be dict!'
    if not isinstance(word, str):
        return 'Word must be str!'
    if len(my_dict) == 0:
        return 'Dictionary is empty!'
    if len(word) == 0:
        return 'Word is empty!'
    if word in my_dict:
        return my_dict.get(word)
    else:
        return F'Can`t find Russian word: {word}'