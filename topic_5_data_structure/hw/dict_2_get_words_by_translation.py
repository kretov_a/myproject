"""
Функция get_words_by_translation.


Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то ‘Can`t find English word: {word}’.

Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
"""
def get_words_by_translation(my_dict, my_word):
    if type(my_dict) != dict:
        return 'Dictionary must be dict!'
    if type(my_word) != str:
        return 'Word must be str!'
    if len(my_dict) == 0:
        return 'Dictionary is empty!'
    if len(my_word) == 0:
        return 'Word is empty!'

    my_list = list(my_dict.keys())
    if my_word in my_list:
        return my_dict[key]
    else:
        return f"Can`t find English word: {my_word}"


def get_words_by_translation(my_dict, my_word):
    if not isinstance(my_dict, dict):
        return 'Dictionary must be dict!'
    if not isinstance(my_word, str):
        return 'Word must be str!'
    if not my_dict:
        return 'Dictionary is empty!'
    if not my_word:
        return 'Word is empty!'
    my_list = []
    for x, y in my_dict.items():
        if my_word in y:
            my_list.append(x)
    if len(my_list) == 0:
        return f'Can`t find English word: {my_word}'
    else: return my_list

     #return f'Can`t find English word: {word}’'