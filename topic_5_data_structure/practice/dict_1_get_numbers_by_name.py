"""
Функция get_numbers_by_name.

Принимает 2 аргумента:
словарь содержащий {имя: телефон},
слово (имя) для поиска в словаре.

Возвращает все телефоны по имени, если такое имя есть в словаре, иначе "Такого имени нет в каталоге!".

Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
Если словарь пустой, то возвращать строку 'Dictionary is empty!'.

Если вместо строки для поиска передано что-то другое, то возвращать строку 'Name must be str!'.
Если строка для поиска пустая, то возвращать строку 'Name is empty!'.
"""
def get_numbers_by_name(phone_book, name):
    if phone_book != dict:
        return 'Dictionary must be dict!'
    if len(phone_book) == 0:
        return 'Dictionary is empty!'
    if name != str:
        return 'Name must be str!'
    if len(name) == 0:
        return 'Name is empty!'
    return phone_book[name]
