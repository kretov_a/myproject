"""
Функция len_first.

Принимает 1 аргумент: кортеж my_tuple.

Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.

Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).

Если вместо tuple передано что-то другое, то возвращать строку 'Must be tuple!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.
"""