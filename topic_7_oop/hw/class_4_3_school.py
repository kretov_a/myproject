"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker

class School():
    def __init__(self,  number, people: list):
        self.number = number
        self.people = people

    def get_avg_mark(self):
        marks = [p.get_avg_mark() for p in self.people if isinstance(p, Pupil)]
        return sum(marks) / len(marks)

    def get_avg_salary(self):
        salary = []
        for i in self.people:
            if type(i) == Worker:
                salary.append(i.salary)
        return sum(salary) / len(salary)

    def get_worker_count(self):
        worker_count = 0
        for i in self.people:
            if type(i) == Worker:
                worker_count += 1
        return worker_count

    def get_pupil_count(self):
        pupil_count = 0
        for i in self.people:
            if type(i) == Pupil:
                pupil_count += 1
        return pupil_count

    def get_pupil_names(self):
        pupil_names = []
        for i in self.people:
            if type(i) == Pupil:
                pupil_names.append(i.name)
        return pupil_names

    def get_unique_worker_positions(self):
        worker_position = []
        for i in self.people:
            if type(i) == Worker:
                worker_position.append(i.position)
        return set(worker_position)

    def get_max_pupil_age(self):
        return max([i.age for i in self.people if type(i) == Pupil])

    def get_min_worker_salary(self):
        return min([i.salary for i in self.people if type(i) == Worker])

    def get_min_salary_worker_names(self):
        min_salary_worker_names = []
        for i in self.people:
            if type(i) == Worker and i.salary == self.get_min_worker_salary():
                min_salary_worker_names.append(i.name)
        return min_salary_worker_names

