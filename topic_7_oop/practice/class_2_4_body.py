from topic_7_oop.practice.class_2_1_heart import Heart
from topic_7_oop.practice.class_2_2_eye import Eye


class Body:
    def __init__(self, eye_left: Eye, eye_right: Eye, heart: Heart, height: float):
        self.eyes = (eye_left, eye_right)
        self.heart = heart
        self.height = height
