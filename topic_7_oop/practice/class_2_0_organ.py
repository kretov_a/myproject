class Organ:
    def __init__(self, healthy: bool, mass: int):
        self.healthy = healthy
        self.mass = mass
