"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""
def count_odd_num(n):
    if type(n) != int:
        return "Must be int!"
    if n <= 0:
        return "Must be > 0!"

    odd = 0
    for i in str(n):
        if int(i) % 2 == 1:
            odd += 1
    return odd




