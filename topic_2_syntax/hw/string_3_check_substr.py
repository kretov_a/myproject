"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""
def check_substr(a,b):
    if len(a) > len(b) and b in a or len(b) > len(a) and a in b:
        return True
    elif len(a) == len(b):
        return False
    else:
        return False
