def triple_string_len(my_str):
    """
    Функция triple_string_len.

    Принимает строку my_str.
    Вернуть эту строку в таком виде: "<my_str>, <my_str>? <my_str>! len=<len(my_str)> :)".

    Если строка пустая, то вернуть None
    """

    if my_str == "":
        return None

    return f"{my_str}, {my_str}? {my_str}! len={len(my_str)} :)"


if __name__ == '__main__':
    triple_string_len('abc')
