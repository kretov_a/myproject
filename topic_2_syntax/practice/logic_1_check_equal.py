def check_equal(x, y, z):
    """
    Функция check_equal.

    Принимает 3 числа.
    Вернуть True, если среди них есть одинаковые, иначе False.
    """

    if type(x) == type(y) == type(z) == int:
        # альтернативный вариант 1:
        # if x == y or y == z or x == z:
        #     return True
        # else:
        #     return False

        # альтернативный вариант 2:
        # return True if x == y or y == z or x == z else False

        return x == y or y == z or x == z

    else:
        return 'Input digit!'


if __name__ == '__main__':
    print(check_equal(1, 2, 1))
    print(check_equal(1, '2', 1))
