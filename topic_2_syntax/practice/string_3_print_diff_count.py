def print_diff_count(str1, str2):
    """
    Функция print_diff_count.

    Принимает две строки.

    Вывести большую по длине строку столько раз,
    насколько символов отличаются строки.

    Если строки одинаковой длины, то вывести строку "Equal!".
    """

    n = len(str1) - len(str2)
    if n == 0:
        print("Equal!")
    elif n < 0:
        print(str2 * abs(n))
    else:
        print(str1 * n)
