import json
import telebot
from telebot import types
from telebot.types import Message

from body_part import BodyPart
from game_result import GameResult
from character import Character
from character_npc import CharacterNPC
from character_by_type import character_by_type
from character_state import CharacterState
from character_type import CharacterType


token = '1661721156:AAGXvkM4Xm83fBcW4lba8c29gHjp6vd-R2U'

bot = telebot.TeleBot('1661721156:AAGXvkM4Xm83fBcW4lba8c29gHjp6vd-R2U')


state = {}


statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def update_save_stat(user_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    if statistics.get(user_id, None) is None:
        statistics[user_id] = {}

    if result == GameResult.W:
        statistics[user_id]['W'] = statistics[user_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[user_id]['L'] = statistics[user_id].get('L', 0) + 1
    elif result == GameResult.E:
        statistics[user_id]['E'] = statistics[user_id].get('E', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError as my_error:
        statistics = {}
        print('файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Hi!\n/start для начала игры\n/stat для отображения статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было ни одной игры"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Готов начать битву ?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Хорошо, начинаем!')

        create_npc(message)

        ask_user_about_character_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Ок, я подожду.')
    else:
        bot.send_message(message.from_user.id,
                         'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    character_npc = CharacterNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_character'] = character_npc

    npc_image_filename = character_by_type[character_npc.character_type][character_npc.name]
    bot.send_message(message.chat.id, 'Противник:')
    with open(f"{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, character_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_character_type(message):
    markup = types.InlineKeyboardMarkup()

    for character_type in CharacterType:
        markup.add(types.InlineKeyboardButton(text=character_type.name,
                                              callback_data=f"character_type_{character_type.value}"))

    bot.send_message(message.chat.id, "Выбери тип персонажа:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "character_type_" in call.data)
def character_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        character_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери своего персонажа:")

        ask_user_about_character_by_type(character_type_id, call.message)


def ask_user_about_character_by_type(character_type_id, message):
    character_type = CharacterType(character_type_id)
    character_dict_by_type = character_by_type.get(character_type, {})

    for character_name, character_img in character_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=character_name,
                                              callback_data=f"character_name_{character_type_id}_{character_name}"))
        with open(f"{character_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "character_name_" in call.data)
def character_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        character_type_id, character_name = int(call_data_split[2]), call_data_split[3]

        create_user_character(call.message, character_type_id, character_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_character(message, character_type_id, character_name):
    print(f"Начало создания объекта Character для chat id = {message.chat.id}")
    global state
    user_character = Character(name=character_name,
                           character_type=CharacterType(character_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_character'] = user_character

    image_filename = character_by_type[user_character.character_type][user_character.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_character)

    print(f"Завершено создание объекта Character для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Выбор точки для защиты:",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбор точки для удара:",
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        # обращение по ключу ко вложенному словарю для получения объекта персонажа пользователя
        user_character = state[message.chat.id]['user_character']

        # обращение по ключу ко вложенному словарю для получения объекта персонажа npc
        character_npc = state[message.chat.id]['npc_character']

        # конвертировать строку хранящуюся в объекте attack_body_part в правильный тип enum BodyPart
        # конвертировать строку хранящуюся в объекте defend_body_part в правильный тип enum BodyPart
        user_character.next_step_points(next_attack=BodyPart[attack_body_part],
                                        next_defence=BodyPart[defend_body_part])

        character_npc.next_step_points()

        game_step(message, user_character, character_npc)


def game_step(message: Message, user_character: Character, character_npc: Character):
    comment_npc = character_npc.get_hit(opponent_attack_point=user_character.attack_point,
                                        opponent_hit_power=user_character.hit_power,
                                        opponent_type=user_character.character_type)
    bot.send_message(message.chat.id, f"NPC character: {comment_npc}\nHP: {character_npc.hp}")

    comment_user = user_character.get_hit(opponent_attack_point=character_npc.attack_point,
                                          opponent_hit_power=character_npc.hit_power,
                                          opponent_type=character_npc.character_type)
    bot.send_message(message.chat.id, f"Your character: {comment_user}\nHP: {user_character.hp}")

    if character_npc.state == CharacterState.READY and user_character.state == CharacterState.READY:
        bot.send_message(message.chat.id, "Продолжаем!")
        game_next_step(message)
    elif character_npc.state == CharacterState.DEFEATED and user_character.state == CharacterState.DEFEATED:
        bot.send_sticker(message.chat.id, 'CAACAgEAAxkBAAECF-tgWda4cApBFf_V1G4VHMXcTInnZAACEQEAAsWInAQvYhLCfroj_B4E')
        bot.send_message(message.chat.id, "Ничья!\nДля продолжения введи /start")
        update_save_stat(message.chat.id, GameResult.E)
    elif character_npc.state == CharacterState.DEFEATED:
        bot.send_sticker(message.chat.id, 'CAACAgEAAxkBAAECF-dgWdZBIrads7c1VRsyuNdhELHIvwACxwADxYicBJZ-kiFhJs07HgQ')
        bot.send_message(message.chat.id, "Ты победил \nДля продолжения введи /start")
        update_save_stat(message.chat.id, GameResult.W)
    elif user_character.state == CharacterState.DEFEATED:
        bot.send_sticker(message.chat.id, 'CAACAgEAAxkBAAECF-lgWdZ8ItFwXVNWFdTd94cDha-7MAAC4QADxYicBOKNRBh5AnZ5HgQ')
        bot.send_message(message.chat.id, "Ты проиграл \nДля продолжения введи /start")
        update_save_stat(message.chat.id, GameResult.L)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')

