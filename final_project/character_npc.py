import random



from character_by_type import character_by_type
from character import Character
from character_type import CharacterType
from body_part import BodyPart

class CharacterNPC(Character):
    def __init__(self):
        rand_type_value = random.randint(CharacterType.min_value(), CharacterType.max_value())
        rand_char_type = CharacterType(rand_type_value)

        rand_char_name = random.choice(list(character_by_type.get(rand_char_type, {}).keys()))

        super().__init__(rand_char_name, rand_char_type)



    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)