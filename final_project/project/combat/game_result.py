from enum import Enum

# победа, проигрыш и ничья
GameResult = Enum("GameResult", "W L E")
