from character_type import CharacterType

character_defence_weaknesses_by_type = {
    CharacterType.NEUTRAL: (CharacterType.NEUTRAL,),
    CharacterType.EVIL: (CharacterType.GOOD,),
    CharacterType.GOOD: (CharacterType.EVIL,)
}