from body_part import BodyPart
from character_state import CharacterState
from character_type import CharacterType
from character_types_weaknesses import character_defence_weaknesses_by_type as weaknesses_by_type


class Character:
    def __init__(self,
                 name: str,
                 character_type: CharacterType):
        self.name = name
        self.character_type = character_type
        self.weaknesses = weaknesses_by_type.get(character_type, tuple())
        self.hp = 40
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 5
        self.state = CharacterState.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.character_type.name}\nHP: {self.hp}"

    def next_step_points(self,
                         next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: CharacterType):
        if opponent_attack_point == BodyPart.NOTHING:
            return "Мимо"
        elif self.defence_point == opponent_attack_point:
            return "Слабак!"
        else:
            self.hp -= opponent_hit_power * (3 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = CharacterState.DEFEATED
                return "Побежден :("
            else:
                return "Ой ой"


