from character_type import CharacterType


character_by_type = {
    CharacterType.NEUTRAL: {'Mr.Poopybuthole': 'mr_poopybuthole.jpg',
                            'Mr.Meeseeks': 'mr_meeseeks.jpg'},


    CharacterType.EVIL: {'Scary Terry': 'scary_terry.jpg',
                         'Prince Nebulon': 'prince_nebulon.jpg'},


    CharacterType.GOOD: {'Morty': 'morty.jpg',
                         'Rick Sanchez': 'rick_sanchez.jpg'},

}
