from enum import Enum, auto


class CharacterType(Enum):
    NEUTRAL = auto()
    EVIL = auto()
    GOOD = auto()

    @classmethod
    def min_value(cls):
        return cls.NEUTRAL.value

    @classmethod
    def max_value(cls):
        return cls.GOOD.value
